import { Component, OnInit } from '@angular/core';
// import {sendEmailVerification  } from "firebase/auth";
import {
  FormGroup,
  FormControl,
  Validators,
  ValidatorFn,
  AbstractControl,
  ValidationErrors,
} from '@angular/forms';
import { Router } from '@angular/router';
import { FirebaseError } from 'firebase/app';
import { catchError, switchMap } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { UsersService } from '../services/users.service';
import { UserProfile } from '../models/Profile.model';

//custom validator for not matching passwords
export function MatchPasswordValidator(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const password = control.get('password')?.value;
    const confirmPassword = control.get('passwordConfirm')?.value;
    if (password && confirmPassword && password !== confirmPassword) {
      return {
        passwordsNotMatching: true,
      };
    }
    return null;
  };
}
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  user$ = this.authSrv.currentUser$;
  constructor(
    private authSrv: AuthService,
    private router: Router,
    private userServ: UsersService
  ) {}

  registerForm = new FormGroup(
    {
      userName: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
      ]),
      passwordConfirm: new FormControl('', [Validators.required]),
    },
    {
      validators: MatchPasswordValidator(),
    }
  );
  ngOnInit(): void {}

  handleRegister(): void {
    if (this.registerForm.invalid) {
      console.log('cant register');
      return;
    }

    console.log('can register');
    const { userName, email, password } = this.registerForm.value;
    //register and verify Email
    this.authSrv
      .register(email, password)
      .pipe(
        switchMap(({ user: { uid } }) =>
          this.userServ.addUser({ uid, email, displayName: userName })
        )
      )
      .subscribe(
        (res) => {
          console.log('response', res);
          // this.authSrv.sendEmailVerification(res.user)
        },
        (err) => alert(err),
        () => this.router.navigate(['/home'])
      );
  }

  get userName() {
    return this.registerForm.get('userName');
  }
  get email() {
    return this.registerForm.get('email');
  }
  get password() {
    return this.registerForm.get('password');
  }
  get passwordConfirm() {
    return this.registerForm.get('passwordConfirm');
  }
}
