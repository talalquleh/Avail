import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from './home-page/home-page.component';
import { ShowProfileComponent } from './show-profile/show-profile.component';
import { ChatPageComponent } from './chat-page/chat-page.component';
import { ActivityPageComponent } from './activity-page/activity-page.component';
import { PostedPageComponent } from './posted-page/posted-page.component';
import { SavedPageComponent } from './saved-page/saved-page.component';
import { SearchPageComponent } from './search-page/search-page.component';
import { UserPageComponent } from './user-page/user-page.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { VerifyEmailComponent } from './verify-email/verify-email.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { canActivate, redirectUnauthorizedTo,redirectLoggedInTo, emailVerified } from '@angular/fire/auth-guard';

const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['login']);
const redirectLoggedInToHome=()=>redirectLoggedInTo(['home'])


const routes: Routes = [
  {
    path: '',
    component: HomePageComponent,
    ...canActivate(redirectUnauthorizedToLogin)
  },
  {
    path: 'home',
    component: HomePageComponent,
    ...canActivate(redirectUnauthorizedToLogin)
  },
  {
    path: 'profile',
    component: ShowProfileComponent,
    ...canActivate(redirectUnauthorizedToLogin)
  },
  {
    path: 'chat',
    component: ChatPageComponent,
    ...canActivate(redirectUnauthorizedToLogin)
  },
  {
    path: 'posted',
    component: PostedPageComponent,
    ...canActivate(redirectUnauthorizedToLogin)
  },
  {
    path: 'activity',
    component: ActivityPageComponent,
    ...canActivate(redirectUnauthorizedToLogin)
  },
  {
    path: 'saved',
    component: SavedPageComponent,
    ...canActivate(redirectUnauthorizedToLogin)
  },
  {
    path: 'search',
    component: SearchPageComponent,
    ...canActivate(redirectUnauthorizedToLogin)
  },
  {
    path: 'user/:id/show',
    component: UserPageComponent,
    ...canActivate(redirectUnauthorizedToLogin)
  },
  {
    path: 'login',
    component: LoginComponent,
    ...canActivate(redirectLoggedInToHome)
  },
  {
    path: 'register',
    component: RegisterComponent,
    ...canActivate(redirectLoggedInToHome)
  },
  {
    path: 'forgotPassword',
    component: ForgotPasswordComponent,
    ...canActivate(redirectLoggedInToHome)
  },
  {
    path: 'verifyEmail',
    component: VerifyEmailComponent,
    ...canActivate(redirectLoggedInToHome)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
