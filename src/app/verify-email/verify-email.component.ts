import { Component, OnInit } from '@angular/core';
import { user } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-verify-email',
  templateUrl: './verify-email.component.html',
  styleUrls: ['./verify-email.component.scss'],
})
export class VerifyEmailComponent implements OnInit {
  user$ = this.authSrv.currentUser$;
  currentUser:any;
  constructor(private authSrv: AuthService, router: Router) {}

  ngOnInit(): void {}
  async handleVerify(){

    this.currentUser = this.user$.pipe();
    console.log(this.currentUser.email);
    
  }
}
