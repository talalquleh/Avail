import { Component, OnInit, OnChanges } from '@angular/core';
import { AuthService } from './services/auth.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnChanges {
  title = 'Avail';
  constructor(public authServ: AuthService) {}
  ngOnChanges(): void {}
}
