import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { UsersService } from '../services/users.service';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  user$ = this.userServ.currentUserProfile$;
  constructor(
    private authServ: AuthService,
    private router: Router,
    private userServ: UsersService
  ) {}

  ngOnInit(): void {
  }

  handleLogout(): void {
    console.log('loging out');
    this.authServ.logout().subscribe(() => {
      this.router.navigate(['/login']);
    });
  }
}
