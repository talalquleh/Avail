import { Injectable } from '@angular/core';
import {
  Firestore,
  doc,
  setDoc,
  updateDoc,
  docData,
} from '@angular/fire/firestore';
import { UserProfile } from '../models/Profile.model';
import { Observable, from, switchMap, of } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  constructor(private firestore: Firestore, private authServ: AuthService) {}

  addUser(user: UserProfile): Observable<any> {
    const doc_ref = doc(this.firestore, 'users', user?.uid);
    return from(setDoc(doc_ref, user));
  }
  updateUser(user: UserProfile): Observable<any> {
    const doc_ref = doc(this.firestore, 'users', user?.uid);
    return from(updateDoc(doc_ref, { ...user }));
  }

  get currentUserProfile$(): Observable<UserProfile | null> {
    return this.authServ.currentUser$.pipe(
      switchMap((user) => {
        if (!user?.uid) {
          return of(null);
        }
        const doc_ref = doc(this.firestore, 'users', user?.uid);
        return docData(doc_ref) as Observable<UserProfile>;
      })
    );
  }
}
