import { Injectable } from '@angular/core';
import { Observable, switchMap, from } from 'rxjs';
import {
  Storage,
  uploadBytes,
  getDownloadURL,
  ref,
} from '@angular/fire/storage';

@Injectable({
  providedIn: 'root',
})
export class ImgsUploadService {
  constructor(private storage: Storage) {}

  uploadImg(img: File, path: string): Observable<string> {
    const Storage_ref = ref(this.storage, path);
    return from(uploadBytes(Storage_ref, img)).pipe(
      switchMap((result) => getDownloadURL(result.ref))
    );
  }
}
