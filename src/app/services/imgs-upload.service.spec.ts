import { TestBed } from '@angular/core/testing';

import { ImgsUploadService } from './imgs-upload.service';

describe('ImgsUploadService', () => {
  let service: ImgsUploadService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ImgsUploadService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
