import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import {
  Auth,
  signInWithEmailAndPassword,
  createUserWithEmailAndPassword,
  signOut,
  authState,
} from '@angular/fire/auth';
import {
  sendEmailVerification,
  sendPasswordResetEmail,
  updateProfile,
  User,
  UserCredential,
  UserInfo,
} from 'firebase/auth';
import { from, Observable, of, concatMap } from 'rxjs';


@Injectable({
  providedIn: 'root',
})
export class AuthService {
  currentUser$ = authState(this.auth);
  constructor(private auth: Auth, private router: Router) {}



  sendEmailVerification(user: User):Observable<void> {
    return from(sendEmailVerification(user));
  }

  // login
  login(email: string, password: string): Observable<UserCredential> {
    //we are changing the return value to observable because we are using it throughout the entire application
    return from(signInWithEmailAndPassword(this.auth, email, password));
  }
  //register
  register(email: string, password: string): Observable<UserCredential> {
    return from(createUserWithEmailAndPassword(this.auth, email, password));
  }
  //sign out
  logout(): Observable<void> {
    return from(this.auth.signOut());
  }
  //forgot password

  forgotPasswd(email: string): Observable<void> {
    return from(sendPasswordResetEmail(this.auth, email));
  }
  signWithGoogle() {}

  updateProfile(profileDate: Partial<UserInfo>): Observable<any> {
    const user = this.auth.currentUser;
    return of(user).pipe(
      concatMap((user) => {
        if (!user) throw new Error('user not Authenticated');
        return updateProfile(user, profileDate);
      })
    );
  }
}
