import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-show-category',
  templateUrl: './show-category.component.html',
  styleUrls: ['./show-category.component.scss'],
})
export class ShowCategoryComponent implements OnInit {
  images = [944, 1011, 984].map((n) => `https://picsum.photos/id/${n}/150/150`);
  constructor() {}

  ngOnInit(): void {}
}
