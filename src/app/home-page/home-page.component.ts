import { Component, OnInit } from '@angular/core';
import { PostShowComponent } from '../post/post-show/post-show.component';
import { AuthService } from '../services/auth.service';
import { UsersService } from '../services/users.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
})
export class HomePageComponent implements OnInit {
  user$ = this.userServ.currentUserProfile$;
  constructor(private authServ: AuthService, private userServ: UsersService) {}

  ngOnInit(): void {}
}
