import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { catchError, of, throwError } from 'rxjs';
import { AuthService } from '../services/auth.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  constructor(private authSrv: AuthService, private router: Router) {}

  loginForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required]),
  });

  ngOnInit(): void {}

  handleLogin(): void {
    if (!this.loginForm.valid) {
      console.log('not valid');
      return;
    }
    const { email, password } = this.loginForm.value;

    this.authSrv.login(email, password).subscribe(
      (res) => console.log('getting a response',res.user),
      (err) => alert(err),
      () => this.router.navigate(['/home'])
    );
    console.log('ysesssss sir');
    
  }

  get email() {
    return this.loginForm.get('email');
  }
  get password() {
    return this.loginForm.get('password');
  }
}
