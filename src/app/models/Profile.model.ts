export interface UserProfile {
    uid:string;
    email?:string;
    displayName?:string;
    photoURL?:string;
    isAdmin?:Boolean;
    phone?:string;
    address?:string,
    CreatedAt?:string;
    lastModifiedAt?:string;
    
}
