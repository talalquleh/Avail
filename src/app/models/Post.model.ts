export interface Post{
    PostID:String;
    Title:String;
    Description?:String;
    userID:string;
    ContactInfo?:String;
    photoURL?:String;
    createdAt:String;
    lastModifiedAt?:String;

}