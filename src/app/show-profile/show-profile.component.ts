import { Component, OnInit } from '@angular/core';
// import { User } from 'firebase/auth';
import { AuthService } from 'src/app/services/auth.service';
import { ImgsUploadService } from 'src/app/services/imgs-upload.service';
import { Observable, concatMap } from 'rxjs';
import { FormControl, FormGroup } from '@angular/forms';
import { UsersService } from 'src/app/services/users.service';
import { untilDestroyed, UntilDestroy } from '@ngneat/until-destroy';
import { UserProfile } from '../models/Profile.model';

@UntilDestroy()
@Component({
  selector: 'app-show-profile',
  templateUrl: './show-profile.component.html',
  styleUrls: ['./show-profile.component.scss'],
})
export class ShowProfileComponent implements OnInit {
  user$ = this.userServ.currentUserProfile$;

  profileForm = new FormGroup({
    uid: new FormControl(''),
    displayName: new FormControl(''),
    phone: new FormControl(''),
    address: new FormControl(''),
  });

  constructor(
    private authServ: AuthService,
    private imgServ: ImgsUploadService,
    private userServ: UsersService
  ) {}

  ngOnInit(): void {
    this.userServ.currentUserProfile$
      .pipe(untilDestroyed(this))
      .subscribe((user) => {
        this.profileForm.patchValue({ ...user });
      });
  }

  uploadImg(event: any, user: UserProfile) {
    this.imgServ
      .uploadImg(event.target.files[0], `imgs/profile/${user.uid}`)
      .pipe(
        concatMap((photoURL) =>
          this.userServ.updateUser({ uid: user.uid, photoURL })
        )
      )
      .subscribe(
        (res) => console.log(res),
        (err) => alert(err),
        () => console.log('updated sucessfully')
      );
  }

  updateProfile() {
    const UpdatedProfileDate = this.profileForm.value;
    this.userServ.updateUser(UpdatedProfileDate).subscribe();
  }
}
